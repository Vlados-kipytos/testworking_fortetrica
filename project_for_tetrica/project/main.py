"""
script was written for company tetrica ( test work )
"""
import itertools


def search_pairs(array, k):
    """
    function for output unique values from list
    """
    return set([(a, b) for a, b in itertools.permutations(array, 2) if a + b == k and a >= b])


def working_with_file(file_name):
    """
    function for working with file
    """
    total_sum = 0
    with open(file_name, 'r') as file:
        file_string = file.readline()
        file_list = file_string.split(',')
        file_list.sort()
        for num, letters in enumerate(file_list):
            sum_number = 0
            num += 1
            letters = letters.replace('"', '')
            for letter in letters.lower():
                number = ord(letter) - 96
                sum_number += number
            total = num * sum_number
            total_sum += total
    print(total_sum)


def get_zeros(n):
    """
    function to count the number of zeros at the end
    """
    Ans = []
    d = 2
    while d * d <= n:
        if n % d == 0:
            Ans.append(d)
            n //= d
        else:
            d += 1
    if n > 1:
        Ans.append(n)
    return Ans.count(5)


def factorial(n):
    """
    function for calculating factorial
    """
    factorial = 1
    while n > 1:
        factorial *= n
        n -= 1
    return factorial


if __name__ == "__main__":
    print(search_pairs([1, 2, 6, 5, 3, 4, 7, 8, 3, 2], 5))
    working_with_file('names.txt')
    value = factorial(20)
    print(get_zeros(value))
